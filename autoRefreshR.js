const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({
        headless: true,
        args: [`--window-size=1920,1080`]
    });
    const page = await browser.newPage();

    await page.setViewport({
        width: 1920,
        height: 1080
    });

    // Login
    await page.goto('https://servicenowsignon.okta.com/login/login.htm?fromURI=%2Fapp%2Ftemplate_saml_2_0%2Fk317zlfESMUHAFZFXMVB%2Fsso%2Fsaml%3FSAMLRequest%3DnZPdbuIwEIVfJfI9%252BTEUqEWQUlhUpNJFQKtVb5BrD61Vx856HKD79JsEWrjYsqu99XyZOXPOZIA817RgWelfzQJ%252BloA%252B2OfaIDtUUlI6wyxHhczwHJB5wZbZ7I7RMGaFs94Kq0mQIYLzypqRNVjm4JbgtkrAw%252BIuJa%252FeF8iiSMIWtC3AhXioGrsLhc1JMK7mKsPrBif8BKF6MdaE9s3zmo94UUQe8kJzD%252Bta6Jqu4%252BitnfR%252B6c235ezhNps8TX7MHm8iRBvVBAkm1gloFk3JhmsEEkzHKVnejyjv966obHc5T5I%252ByA697myAtuNESPncvapAnHNEtYXTp4glTA16bnxKaJxct2LaSuJV0mO0z5JeSDvdJxLMjxbdKCOVebns5%252FMBQna7Ws1b8%252B%252FLVdNgqyS4%252B4r%252BFysfwWFjY9WQDAdNjKxR686TvSyEf8RJhn9NYxCdzzhOLFitdzqeW63Ee5BpbXcjB1VeKfGuhCaOnPuvZSRh0rwo2do0KIOcK51J6QCRRJ%252BDjmcLssm2uj8Pex%252BMbF5wp7B2AvZc%252BE8vzrGRrjZdwOa%252FnLmICSbq3tVzfTk762R9CSAqnSvHDRbW%252BQ%252Fn%252FqRoeCx%252Bsd%252BpfP7rDn8D%26RelayState%3Dhttps%253A%252F%252Fdeveloper.servicenow.com%252Fsaml_redirector.do%253Fsysparm_nostack%253Dtrue%2526sysparm_uri%253D%25252Fnav_to.do%25253Furi%25253D%2525252Fssologin.do%2525253FrelayState%2525253D%252525252Fapp.do%2525252523%21%252525252Fdashboard');
    await page.waitForSelector('#username')
    await page.type('#username', '*YOUR EMAIL HERE*');
    await page.type('#password', '*YOUR PASSWORD HERE*');
    page.keyboard.press('Enter'),
    // Wait for Login
    await page.waitForSelector('#dp-hdr-userinfo-link')
    // Navigate to Instance Page
    await page.goto('https://developer.servicenow.com/app.do#!/instance');
    // Click Refresh Status
    await page.waitForSelector('#refresh_status')
    page.click('#refresh_status');
    await page.waitFor(60000);
    browser.close();
})();